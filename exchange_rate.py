import requests
import re

urls = {
    "minfin_mb": "https://minfin.com.ua/ua/currency/mb/",
    "minfin_dp": {
         "usd": "https://minfin.com.ua/currency/auction/usd/buy/dnepropetrovsk/",
         "eur": "https://minfin.com.ua/currency/auction/eur/buy/dnepropetrovsk/"
                },
    "brent-oil": "https://ru.investing.com/commodities/brent-oil",
    "gold_price": "https://tables.finance.ua/ua/metals/cash/-/ua/xau/10#2:0",
    "world_gas_price": "https://index.minfin.com.ua/ua/markets/gas/"
    }
re_patterns = {
            "minfin_mb": {
                "usd": r"<td data-title=\"Доллар\" class=\"active\">(\d+\.\d+)",
                "eur": r"<td data-title=\"Евро\">(\d+\.\d+)"
            },
            "minfin_dp": {
                "buy": r"class=\"buy\".*>(\d+(?:\,|\.)\d+).*class=\"sale\"",
                "sell": r"class=\"sale\".*>(\d+(?:\,|\.)\d+).*button title"
            },
            "brent-oil": r"id=\"last_last\" dir=\"ltr\">(\d+\,\d+)",
            "gold_price": r"class=\"summary\"><td rowspan=\"2\"></td><td><div><em>(\d{4,}\.</em>\d{2,})<img width=\"10\" height=\"10\" border=\"0\" src=\"//static\.finance\.ua/.*/> <em>\d+\.</em>\d+<img width=\"10\" height=\"10\" border=\"0\" src=\"//static\.finance\.ua/.*/></div></td><td><div><em>(\d{4,}\.</em>\d{2,})",
            "world_gas_price": r"\<big\>(\d+[\,\.]\d+)\<\/big\>.*USD\s\/\sMMBtu"
            }

error_message = "нет данных"


def generate_message():

    def _get_value(url,re_pattern):
        r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
        html_txt = r.text
        value = re.findall(re_pattern, html_txt)
        return value

    try:
        minfin_mb_usd_buy = _get_value(urls["minfin_mb"], re_patterns["minfin_mb"]["usd"])[0]
    except IndexError:
        minfin_mb_usd_buy = error_message
    try:
        minfin_mb_usd_sell = _get_value(urls["minfin_mb"], re_patterns["minfin_mb"]["usd"])[1]
    except IndexError:
        minfin_mb_usd_sell = error_message
    try:
        minfin_mb_eur_buy = _get_value(urls["minfin_mb"], re_patterns["minfin_mb"]["eur"])[0]
    except IndexError:
        minfin_mb_eur_buy = error_message
    try:
        minfin_mb_eur_sell = _get_value(urls["minfin_mb"], re_patterns["minfin_mb"]["eur"])[1]
    except IndexError:
        minfin_mb_eur_sell = error_message
    try:
        minfin_dp_usd_buy = _get_value(urls["minfin_dp"]["usd"], re_patterns["minfin_dp"]["buy"])[0]
    except IndexError:
        minfin_dp_usd_buy = error_message
    try:
        minfin_dp_usd_sell = _get_value(urls["minfin_dp"]["usd"], re_patterns["minfin_dp"]["sell"])[0]
    except IndexError:
        minfin_dp_usd_sell = error_message
    try:
        minfin_dp_eur_buy = _get_value(urls["minfin_dp"]["eur"], re_patterns["minfin_dp"]["buy"])[0]
    except IndexError:
        minfin_dp_eur_buy = error_message
    try:
        minfin_dp_eur_sell = _get_value(urls["minfin_dp"]["eur"], re_patterns["minfin_dp"]["sell"])[0]
    except IndexError:
        minfin_dp_eur_sell = error_message

    try:
        brent_oil = _get_value(urls.get("brent-oil"), re_patterns.get("brent-oil"))[0]
    except IndexError:
        brent_oil = error_message

    try:
        gold_price_buy = _get_value(urls.get("gold_price"), re_patterns.get("gold_price"))[0][0]
    except IndexError:
        gold_price_buy = error_message
    try:
        gold_price_sell = _get_value(urls.get("gold_price"), re_patterns.get("gold_price"))[0][1]
    except IndexError:
        gold_price_sell = error_message
    try:
        world_gas_price = _get_value(urls.get("world_gas_price"), re_patterns.get("world_gas_price"))[0]
    except IndexError:
        world_gas_price = error_message

    message = "Курс валют:\n"\
                       f"Межбанк доллар: покупка - {minfin_mb_usd_buy}; продажа - {minfin_mb_usd_sell};\n"\
                       f"Черный рынок доллар: покупка - {minfin_dp_usd_buy}; продажа - {minfin_dp_usd_sell}\n"\
                       f"Нефть - {brent_oil} за баррель\n"\
                       f"Межбанк евро: покупка - {minfin_mb_eur_buy}; продажа - {minfin_mb_eur_sell};\n"\
                       f"Черный рынок евро: покупка - {minfin_dp_eur_buy}; продажа - {minfin_dp_eur_sell};\n"\
                       f"золото за 1г НБУ: покупка - {re.sub(r'</em>', '',gold_price_buy)}; продажа - {re.sub(r'</em>', '', gold_price_sell)}.\n"\
                       f"Цена на газ - {world_gas_price}$ (Нью-Йорская товарная биржа)"

    return message, minfin_mb_usd_buy

if __name__ == '__main__':
    print(generate_message())

    message, usd = generate_message()
    print(type(usd), usd)