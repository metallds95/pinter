import requests

def send_mess(chat, text, token):
    params = {'chat_id': chat, 'text': text}
    response = requests.post(f"https://api.telegram.org/bot{token}/" + 'sendMessage', data=params)
    return response
