import re
import requests
import datetime
from operator import itemgetter

####### vars
today = datetime.date.today().strftime("%Y-%m-%d")
root_url = "https://agrotender.com.ua/traders"
# end_url_prefix = "?currency=uah"
region_url_prefix = (("region_nikolaev", "Николаевская обл. Порт"), ("region_odessa", "Одесская обл. Порт"))
required_rubrics = {
    'Рапс': "raps",
    'Рапс с ГМО': 'raps_gmo',
    'Пшеница 2 кл.': "pshenica_2_kl",
    'Пшеница 3 кл.': "pshenica_3_kl",
    'Пшеница 4 кл.': "pshenica_4_kl",
    'Ячмень': "yachmen",
    'Горох желтый': "goroh_zheltyy",
    'Кукуруза': "kukuruza",
    'Подсолнечник': "podsolnechnik"
}
required_ports = (
    'Ника-Тера',
    'Николаевский МП',
    'Николаевский РечПорт',
    'Одесский МП',
    'Черноморский МП',
    'Южный МП'
)

def generate_message(usd_buy):
    def _fetch_prices(url):
        r = requests.get(url)
        html_txt = r.text
        data = re.findall(
            r"\"title\">\s+(?P<company>.*)"
            r"|price.*>(?P<price>\d+)<"
            r"|data-date=.*>(?P<date>\d+-\d+-\d+)<"
            r"|location\">(\w+(?:\s\w+)?)",
            html_txt)
        # prepare coordinates matrix
        coordinates = [[], []]
        separated_arrays = []
        # get companies coordinates in request result array
        for i in range(len(data)):
            if data[i][0]:
                coordinates[0].append(i)
            if data[i][-1]:
                coordinates[1].append(i)
        # get separated array by companies arrays
        for i in range(len(coordinates[0])):
            # get company data according to coordinates in request result
            tmp_list_array = data[coordinates[0][i]:coordinates[1][i] + 1]
            company_list = []
            for tuple_array in tmp_list_array:
                for element in tuple_array:
                    if element:
                        company_list.append(element)
            # check array and remove USD price if company has UAH price
            for index, value in enumerate(company_list):
                try:
                    if len(value) < 4 and isinstance(int(value), int):
                        # remove USD if company has UAH price (means that with USD and UAH array will contain 5 elements)
                        if len(company_list) > 4:
                            company_list.remove(value)
                        # if there is only USD then convert to UAH
                        else:
                            company_list[index] = int(int(value) * float(usd_buy))
                except: continue

            separated_arrays.append(company_list)

        # get prices only for required ports
        prices = [x for x in separated_arrays if x[3] in required_ports and x[2] == today]          # arary example that we should have here:
                                                                                                # ['Ферко', '9100', '2021-01-27', 'Одесский МП']
                                                                                                # ['CORETRADE', '9450', '2021-01-27', 'Одесский МП']
                                                                                               # ['Системтрейд', '9200', '2021-01-25', 'Одесский МП']
        # here we have an int converting for better price sorting
        for i in range(len(prices)):
            prices[i][1] = int(prices[i][1])

        return prices


### generate price message
    message = ""
    message += f"Цены за {today}\n"
    for reg_url in region_url_prefix:
        message += f"\n{reg_url[1]}:\n"
        for (rubric, v) in required_rubrics.items():
            full_url = root_url + "/" + reg_url[0] + "/" + v
            result = sorted(_fetch_prices(full_url), key=itemgetter(1), reverse=True)
            if len(result) > 0:
                message += f"{rubric} = {result[0][1]} ({result[0][3]})({result[0][0]})\n"
            else:
                message += f"{rubric} = Цены нет\n"
    ### just example of full_url = "https://agrotender.com.ua/traders/region_nikolaev/pshenica_3_kl"
    ### current version - https://agrotender.com.ua/traders/tport_odesskij/pshenica_2_kl (they have removed exchange option)

    return message


if __name__ == '__main__':
    print(generate_message(27.9400))
