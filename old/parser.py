#!/usr/bin/env python
import datetime
import requests
import re

####### vars
today = datetime.date.today().strftime("%d.%m.%Y")
nikolaev_url = "https://agrotender.com.ua/traders/region_nikolaev/index.html"
odessa_url = "https://agrotender.com.ua/traders/region_odessa/index.html"
c_url = "https://agrotender.com.ua"
required_rubrics = {'Рапс': set(), 'Пшеница 2 кл.': set(), 'Пшеница 3 кл.': set(), 'Пшеница 4 кл.': set(), 'Ячмень': set(), 'Горох желтый': set(), 'Кукуруза': set(), 'Подсолнечник': set()}




####### functions
def get_companies_url(url):
    r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    html_txt = r.text
    companies = re.findall(r"<a href=\"(/kompanii/comp-\d+\.html)\">(\w+.*)<", html_txt)
    return companies

def get_companies_data(url):
    r = requests.get(f"{url}", headers={'User-Agent': 'Mozilla/5.0'})
    html_txt = r.text
    table = re.findall(r"<b>Обновлено\s\d+\.\d+\.\d+</b>(?:\n.*){1,}regions-tabs", html_txt)
    if len(table) > 0:
        table_txt = table[0]
        updated_date = re.findall(r"Обновлено\s(\d+\.\d+\.\d+)", table_txt)
        if len(updated_date) > 0:
            if updated_date[0] == today:
                rubrics = re.findall(r"<th rubric=\"(\d+)\">(\w+.*)<", table_txt)
                titles = re.findall(r"<td place=\"(\d+)\".*\n.*\"place-title\">(\w+.*)<.*\n.*\"place-comment\">(\w+.*)?<", table_txt)
                data = {x: {y: None for y in rubrics} for x in titles}
                for t in titles:
                    for r in rubrics:
                        price = re.findall(f"<td place=\"{int(t[0])}\" rubric=\"{int(r[0])}\".*currency-0\">\n(?:.*<div.*>(\d+)<)?", table_txt)
                        data[t][r] = price
                return data
    else:
        return None

def all_data(url):
    companies = get_companies_url(url)
    result_data = {}
    for i in companies:
        data = get_companies_data(c_url + i[0])
        if data:
            result_data[i[1]] = data
        else:
            continue
    return result_data

def print_result(data):
    for company in data.keys():
        for port in data[company]:
            for (rubric, value) in data[company][port].items():
                if rubric[1] in required_rubrics.keys():
                    try:
                        required_rubrics[rubric[1]].add(value[0])
                    except IndexError:
                        pass
    for (k, v) in required_rubrics.items():
        v_sorted = sorted(v, reverse=True)
        try:
            if len(v_sorted) == 0:
                print(f"- {k} = Цены нет;")
            else:
                print(f"- {k} = {v_sorted[0]};")
        except IndexError:
            pass

############################    RUN    ############################
print("##### Николаевская обл. Порт")
print_result(all_data(nikolaev_url))
print("#########################\n\n")
print("##### Одесская обл. Порт")
print_result(all_data(odessa_url))